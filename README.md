# Devops Challenge

Le shéma ci-dessous résume l'architecture cloud de ma solution : 
![image info](./assets/devops-challenge-diagram.png)

### Explication des composants utilisés :

- **API Gateway :** c'est le point d'entrée de la page de recherche et qui déclenchera par la suite la fonction lambda
- **Lambda Funtion :** elle reçois la requête de recherche de la l'API Gateway et la connectera à un flux de données Amazon Kinesis
- **Kinesis Data Stream :** il se charge de stocker toutes les requêtes de recherche enregistrées en temps réel et permettra l'analyse de ces données
- **Amazon Kinesis Data Analytics :** cela nous permettra d'effectuer des analyses en temps réel sur les données des requêtes de recherche et nous permettra d'identifier les marques les plus fréquemment recherchées
- **Amazon S3 :** c'est pour stocker les données brutes des requêtes de recherche qui nous servira pour faire des traitement dans l'avenir
- **Amazon QuickSight :** c'est pour visualiser les données et les afficher dans un tableau de bord pour faciliter aux commerciaux la visualisation et les statistiques sur les marques recherchées
